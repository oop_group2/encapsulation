/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.shapeproject;

/**
 *
 * @author Melon
 */
public class Square {
    private double side;
    public Square(double side) {
        this.side = side;
    }
    public double SquareArea() {
        return side*side;
    }
    public void setSquare(double side) {
        if(side<=0) {
            System.out.println("Error : Side must more than zero!!!");
            return;
        }
        this.side = side;
    }
    public double getSquare() {
     return side;   
    } 
    @Override
    public String toString() {
        return "Area of Square (side = " + this.getSquare() + ") is " + this.SquareArea();
    }            
}
