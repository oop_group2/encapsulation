/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.shapeproject;

/**
 *
 * @author Melon
 */
public class Triangle {
    private double h;
    private double b;
    public Triangle(double h, double b) {
        this.h = h;
        this.b = b;        
    }
    public double TriArea() {
        return (h*b)/2;
    }
    
    public void setTri(double h, double b) {
        if(h<=0 || b<=0) {
            System.out.println("Error: Height and Base must more than zero!!!");
            return;
        }
        this.h = h;
        this.b = b;
    }
    public double geth() {
      return h;
    } 
    public double getb() {
        return b;
    }
    @Override
    public String toString() {
        return "Area of Taiangle ( Height = " + this.geth() + "Base = " + this.getb() + ") is " + this.TriArea();
    }
}
