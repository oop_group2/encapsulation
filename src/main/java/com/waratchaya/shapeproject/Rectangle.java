/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.shapeproject;

/**
 *
 * @author Melon
 */
public class Rectangle {
    private double l;
    private double w;
    public Rectangle(double l , double w) {
        this.l = l;
        this.w = w;
    }

    Rectangle() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public double RecArea() {
        return l*w;
    }
    public void SetRec(double l,double w) {
        if(w<=l) {
            System.out.println("Error : Width must not be greater than Length ");
            return;
        }
        else if(w<=0 || l<=0) {
            System.out.println("Error : Width or Length must more than zero!!!");
            return;
        }
        else if(w==l || l==w) {
            System.out.println("Error : Width and Length should not be the same");
            return;
        }
        
        this.l = l;
        this.w = w;
    } 
    public double getl() {
        return l;
    }
    public double getw() {
        return w;
    }

    /**
     *
     * @return
     */
    public String toSring() {
        return "Area of rectangle1(length = " + this.getl() + ", width = " + this.getw() + ") is " + this.RecArea();
    }
}
